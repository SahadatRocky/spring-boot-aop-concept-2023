package com.example.aop.springaop.aspect;

import com.example.aop.springaop.model.Department;
import com.example.aop.springaop.model.Employee;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/*

Aspect is a class that implements enterprise application concerns that cut across multiple classes.
such as transaction management, logging , security

Join point is a point during the execution of the program,
such as the execution of a method or handling of an exception.

Pointcut predicate that matches join points.
Advice is associated with a pointcut expression and runs at any join point matched by the pointcut.
Spring framework uses the AspectJ pointcut expression language.

Advice : Advice is actions taken for a particular join point.

Before Advice : Advice that executes before a join point.

After Returning Advice : Advice to be executed after a join point completes.

After Throwing Advice : Advice to be executed if a method exits by throwing an exception.

After Advice : Advice to be executed regardless of the means a join point exits (normal or exception return).

Around Advice : Advice that surrounds a join point such as a method invocation.This is a most powerful  kind of advice.

 */

@Aspect
@Slf4j
@Component
public class GeneralInterceptorAspect {

    //@Pointcut("execution(* com.example.aop.springaop.controller.*.*(..))")
    //@Pointcut("within(com.example.aop.springaop..*)")
    //@Pointcut("this(com.example.aop.springaop.service.DepartmentService)")
    @Pointcut("@annotation(com.example.aop.springaop.annotation.CustomAnnotation)")
    public void loggingPointCut(){}

    /*@Before("loggingPointCut()")
    public void before( JoinPoint joinPoint ){
        log.info("Before method invoked::"+joinPoint.getSignature());
    }

    @AfterReturning(value = "execution(* com.example.aop.springaop.controller.*.*(..))",
            returning = "employee")
    public void after( JoinPoint joinPoint, Employee employee ){
        log.info("After method invoked::"+employee);
    }

    @AfterThrowing(value = "execution(* com.example.aop.springaop.controller.*.*(..))",
            throwing = "e")
    public void after( JoinPoint joinPoint, Exception e ){
        log.info("After method invoked::"+e.getMessage());
    }*/


    @Around("loggingPointCut()")
    public Object around( ProceedingJoinPoint joinPoint ) throws Throwable {
        log.info("Before method invoked::"+joinPoint.getArgs()[0]);
        Object object = joinPoint.proceed();

        if(object instanceof Employee){
            log.info("After method invoked..."+joinPoint.getArgs()[0]);
        }else if(object instanceof Department){
            log.info("After method invoked..."+joinPoint.getArgs()[0]);
        }

        return object;
    }

}

